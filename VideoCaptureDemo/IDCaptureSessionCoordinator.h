//
//  IDCaptureSessionCoordinator.h
//  VideoCaptureDemo
//
//  Created by Adriaan Stellingwerff on 1/04/2015.
//  Copyright (c) 2015 Infoding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef NS_ENUM(NSInteger, FocusMode)
{
    FocusModeAuto = 0,
    FocusModeContinuous,
    FocusModeManual,
};

typedef NS_ENUM(NSInteger, ExposureMode)
{
    ExposureModeLocked = 0,
    ExposureModeAuto,
    ExposureModeContinuous,
    ExposureModeCustom
};

typedef NS_ENUM(NSInteger, ResolutionMode)
{
    ResolutionMode4K = 0,
    ResolutionModeFHD,
    ResolutionModeHD
};

extern const double INVALID_LENS_POSITION;
extern const double PINCH_VELOCITY_FACTOR;
extern const double TARGET_BIAS;
extern const double MIN_ZOOM_FACTOR;

@protocol IDCaptureSessionCoordinatorDelegate;

@interface IDCaptureSessionCoordinator : NSObject

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureDeviceInput *cameraDeviceInput;
@property (nonatomic, strong) dispatch_queue_t delegateCallbackQueue;
@property (nonatomic, weak) id<IDCaptureSessionCoordinatorDelegate> delegate;

- (void)setDelegate:(id<IDCaptureSessionCoordinatorDelegate>)delegate callbackQueue:(dispatch_queue_t)delegateCallbackQueue;

- (BOOL)addInput:(AVCaptureDeviceInput *)input toCaptureSession:(AVCaptureSession *)captureSession;
- (BOOL)addOutput:(AVCaptureOutput *)output toCaptureSession:(AVCaptureSession *)captureSession;

- (void)startRunning;
- (void)stopRunning;

- (void)startRecording;
- (void)stopRecording;

- (void)enableRecordAudio:(BOOL)enabled;

- (AVCaptureVideoPreviewLayer *)previewLayer;

- (void)previewLayerWithOrientation:(AVCaptureVideoOrientation)orientation;

- (void)focusWithMode:(FocusMode)focusMode exposeWithMode:(ExposureMode)exposureMode atPointOfInterest:(CGPoint)pointOfInterest monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange;

- (void)torchWithMode:(AVCaptureTorchMode)mode;

- (void)changeCamera;
- (void)exposureWithMode:(ExposureMode)exposureMode targetBias:(float)targetBias duration:(CMTime)duration ISO:(float)ISO;

- (NSString *)currentResolutionImageName;
- (void)changeVideoResolutionWithMode:(ResolutionMode)resolutionMode;

- (float)minISO;
- (float)maxISO;
- (float)minTargetBias;
- (float)maxTargetBias;
- (float)minZoom;
- (float)maxZoom;
- (float)currentZoom;
- (BOOL)isFocusFocusModeLockedSupported;
- (CGFloat)lensPosition;
- (void)updateLensPosition:(CGFloat)position;
- (void)zoomCameraWithVelocity:(CGFloat)velocity;
- (void)zoomCameraWithZoomFactor:(CGFloat)zoomFactor;

@end

@protocol IDCaptureSessionCoordinatorDelegate <NSObject>

@required

- (void)coordinatorDidBeginRecording:(IDCaptureSessionCoordinator *)coordinator;
- (void)coordinator:(IDCaptureSessionCoordinator *)coordinator didFinishRecordingToOutputFileURL:(NSURL *)outputFileURL error:(NSError *)error;

@end
