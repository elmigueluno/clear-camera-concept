//
//  IDCaptureSessionCoordinator.m
//  VideoCaptureDemo
//
//  Created by Adriaan Stellingwerff on 1/04/2015.
//  Copyright (c) 2015 Infoding. All rights reserved.
//

#import "IDCaptureSessionCoordinator.h"

const double INVALID_LENS_POSITION = -1;
const double PINCH_VELOCITY_FACTOR = 5.0;
const double TARGET_BIAS = 2;
const double MIN_ZOOM_FACTOR = 1;

@interface IDCaptureSessionCoordinator ()

@property (nonatomic, strong) dispatch_queue_t sessionQueue;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic) AVCaptureFocusMode currentFocusMode;

@property (nonatomic, strong) NSArray<AVCaptureDevice *> *videoDevices;
//@property (nonatomic, strong) AVCaptureDeviceDiscoverySession *discoverySession;

@end

@implementation IDCaptureSessionCoordinator


- (instancetype)init
{
    self = [super init];
    if (self) {
        NSMutableArray<AVCaptureDeviceType>* deviceTypes = [[NSMutableArray alloc] initWithObjects:AVCaptureDeviceTypeBuiltInWideAngleCamera, AVCaptureDeviceTypeBuiltInTelephotoCamera, nil];
//        [deviceTypes addObject:AVCaptureDeviceTypeBuiltInDualCamera];
        [deviceTypes addObject:AVCaptureDeviceTypeBuiltInWideAngleCamera];
        [deviceTypes addObject:AVCaptureDeviceTypeBuiltInTelephotoCamera];
        /*if (@available(iOS 11.1, *)) {
            [deviceTypes addObject:AVCaptureDeviceTypeBuiltInTrueDepthCamera];
        }*/
        AVCaptureDeviceDiscoverySession *discoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:deviceTypes mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionUnspecified];
        _videoDevices = discoverySession.devices;
        _sessionQueue = dispatch_queue_create("com.example.capturepipeline.session", DISPATCH_QUEUE_SERIAL);
        _captureSession = [AVCaptureSession new];
        [self setupCaptureSession];
        [self addObservers];
    }
    return self;
}

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:_cameraDeviceInput.device];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)subjectAreaDidChange:(NSNotification*)notification
{
    CGPoint point = CGPointMake(0.5, 0.5);
    [self focusWithMode:FocusModeContinuous exposeWithMode:ExposureModeContinuous atPointOfInterest:point monitorSubjectAreaChange:NO];
}

- (void)previewLayerWithOrientation:(AVCaptureVideoOrientation)orientation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _previewLayer.connection.videoOrientation = orientation;
    });
}

- (void)focusWithMode:(FocusMode)focusMode exposeWithMode:(ExposureMode)exposureMode atPointOfInterest:(CGPoint)pointOfInterest monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    AVCaptureFocusMode captureFocusMode = [self captureFocusModeFor:focusMode];
    AVCaptureExposureMode captureExposureMode = [self captureExposureModeFor:exposureMode];
    CGPoint point = [_previewLayer captureDevicePointOfInterestForPoint:pointOfInterest];
    
    dispatch_async(_sessionQueue, ^{
        AVCaptureDevice* device = _cameraDeviceInput.device;
        NSError* error = nil;
        if ([device lockForConfiguration:&error]) {
            /*
             Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
             Call set(Focus/Exposure)Mode() to apply the new point of interest.
             */
            if (device.isFocusPointOfInterestSupported && [device isFocusModeSupported:captureFocusMode]) {
                device.focusPointOfInterest = point;
                device.focusMode = captureFocusMode;
                
                if (captureFocusMode != AVCaptureFocusModeLocked) {
                    [device setFocusMode:captureFocusMode];
                }
            }
            
            if (device.isExposurePointOfInterestSupported && [device isExposureModeSupported:captureExposureMode]) {
                device.exposurePointOfInterest = point;
                device.exposureMode = captureExposureMode;
                [device setExposureMode:captureExposureMode];
            }
            
            device.subjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange;
            [device unlockForConfiguration];
        }
        else {
            NSLog(@"Could not lock device for configuration: %@", error);
        }
    });
}

- (AVCaptureFocusMode)captureFocusModeFor:(FocusMode)focusMode {
    AVCaptureFocusMode captureFocusMode;
    switch (focusMode) {
        case FocusModeAuto:
            captureFocusMode = AVCaptureFocusModeAutoFocus;
            break;
            
        case FocusModeContinuous:
            captureFocusMode = AVCaptureFocusModeContinuousAutoFocus;
            break;
            
        case FocusModeManual:
            captureFocusMode = AVCaptureFocusModeLocked;
            break;
    }
    
    return captureFocusMode;
}

- (AVCaptureExposureMode)captureExposureModeFor:(ExposureMode)exposureMode {
    AVCaptureExposureMode captureExposureMode;
    switch (exposureMode) {
        case ExposureModeAuto:
            captureExposureMode = AVCaptureExposureModeAutoExpose;
            break;
            
        case ExposureModeCustom:
            captureExposureMode = AVCaptureExposureModeCustom;
            break;
            
        case ExposureModeLocked:
            captureExposureMode = AVCaptureExposureModeLocked;
            break;
            
        case ExposureModeContinuous:
            captureExposureMode = AVCaptureExposureModeContinuousAutoExposure;
            break;
    }
    
    return captureExposureMode;
}

- (void)torchWithMode:(AVCaptureTorchMode)mode
{
    if ([_cameraDeviceInput.device hasTorch]) {
        NSError *error;
        [_cameraDeviceInput.device lockForConfiguration:&error];
        if (!error) { // TODO: Notifiy user is fails
            if (mode == AVCaptureTorchModeOn) {
                [_cameraDeviceInput.device setTorchModeOnWithLevel:0.1 error:&error];
                
                if (error) { // TODO: Notifiy user is fails
                    NSLog(@"%s setTorchModeOnWithLevel error: %@", __func__, error.description);
                }
            } else {
                _cameraDeviceInput.device.torchMode = mode;
            }
        } else {
            NSLog(@"%s lockForConfigurationTorch error: %@", __func__, error.description);
        }
        [_cameraDeviceInput.device unlockForConfiguration];
    }
}

- (void)changeCamera
{
    __block int currentDeviceIndex = 0;
    [_videoDevices enumerateObjectsUsingBlock:^(AVCaptureDevice * _Nonnull device, NSUInteger idx, BOOL * _Nonnull stop) {
        if (device == _cameraDeviceInput.device) {
            currentDeviceIndex = (int)idx;
            *stop = YES;
        }
    }];
    
    NSError *error;
//    AVCaptureDeviceInput *currentCameraInput = [AVCaptureDeviceInput deviceInputWithDevice:_cameraDevice error:&error];
    NSLog(@"Error: %@", error.description);
    [_captureSession beginConfiguration];
    [_captureSession removeInput:_cameraDeviceInput];
    AVCaptureDevice *device;
    currentDeviceIndex = (currentDeviceIndex + 1) == _videoDevices.count ? 0 : currentDeviceIndex + 1;
    for (int i = currentDeviceIndex; i < _videoDevices.count; i++) {
        device = _videoDevices[i];
        [self changeVideoResolutionWithMode:[self resolutionModeFromSessionPreset:[self supportedSessionPresetForDevice:device]] device:device];
        AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:NULL];
        if (![self addInput:deviceInput toCaptureSession:_captureSession]) {
            continue;
        }
        _cameraDeviceInput = deviceInput;
    }
    /*for (AVCaptureDevice *device in _videoDevices) {
        if (device.position != _cameraDeviceInput.device.position) {
            [self changeVideoResolutionWithMode:[self resolutionModeFromSessionPreset:[self supportedSessionPresetForDevice:device]] device:device];
            AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:NULL];
            [self addInput:deviceInput toCaptureSession:_captureSession];
            _cameraDeviceInput = deviceInput;
            break;
        }
    }*/
    [_captureSession commitConfiguration];
    
    /*NSArray<AVCaptureDeviceType>* deviceTypes = @[AVCaptureDeviceTypeBuiltInWideAngleCamera, AVCaptureDeviceTypeBuiltInTelephotoCamera];
    AVCaptureDeviceDiscoverySession *discovery = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:deviceTypes mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionUnspecified];
    
    __block NSError *error;
    __block AVCaptureDeviceInput *currentCameraInput;
    [discovery.devices enumerateObjectsUsingBlock:^(AVCaptureDevice * _Nonnull device, NSUInteger idx, BOOL * _Nonnull stop) {
        if (device.position != _cameraDevice.position) {
            
            currentCameraInput = [AVCaptureDeviceInput deviceInputWithDevice:_cameraDevice error:&error];
            [_captureSession removeInput:currentCameraInput];
            
            if (!error && [self addInput:[AVCaptureDeviceInput deviceInputWithDevice:device error:&error] toCaptureSession:_captureSession]) {
                
                if (error) {
                    NSLog(@"Error: %@", error.description);
                }
                
            } else {
                NSLog(@"Error: %@", error.description);
            }
            *stop = YES;
        }
    }];*/
}

- (void)exposureWithMode:(ExposureMode)exposureMode targetBias:(float)targetBias duration:(CMTime)duration ISO:(float)ISO
{
    AVCaptureDevice *device = _cameraDeviceInput.device;
    NSError *error;
    if ([device isExposureModeSupported:[self captureExposureModeFor:exposureMode]]) {
        [device lockForConfiguration:&error];
        if (exposureMode != ExposureModeCustom) {
            if (targetBias >= device.minExposureTargetBias && targetBias <= device.maxExposureTargetBias) {
                [device setExposureTargetBias:targetBias completionHandler:^(CMTime syncTime) {
                    NSLog(@"%s: %lld", __func__, syncTime.value);
                }];
            } else {
                //TODO: Invalid target bias
            }
        } else {
            if (ISO >= [self minISO] && ISO <= [self maxISO]/* && CMTimeCompare(duration, device.activeFormat.maxExposureDuration) == 1 && CMTimeCompare(duration, device.activeFormat.maxExposureDuration) == -1*/) {
                [device setExposureModeCustomWithDuration:duration ISO:ISO completionHandler:^(CMTime syncTime) {
                    NSLog(@"%s: %lld", __func__, syncTime.value);
                }];
            } else {
                //TODO: Invalid ISO
            }
        }
        [device unlockForConfiguration];
    }
}

- (void)changeVideoResolutionWithMode:(ResolutionMode)resolutionMode
{
    [self changeVideoResolutionWithMode:resolutionMode device:_cameraDeviceInput.device];
}

- (NSString *)currentResolutionImageName
{
    if (_captureSession.sessionPreset == AVCaptureSessionPreset3840x2160) {
        return @"4k";
    } else if (_captureSession.sessionPreset == AVCaptureSessionPreset1920x1080) {
        return @"1080p";
    } else {
        return @"720p";
    }
}

- (void)changeVideoResolutionWithMode:(ResolutionMode)resolutionMode device:(AVCaptureDevice *)device {
    [self device:device supportWidth:[self widthForResolutionMode:resolutionMode] completionHandler:^(BOOL supported) {
        if (supported) {
            switch (resolutionMode) {
                case ResolutionMode4K:
                    if ([_captureSession canSetSessionPreset:AVCaptureSessionPreset3840x2160]) {
                        [_captureSession setSessionPreset:AVCaptureSessionPreset3840x2160];
                    } else {
                        [_captureSession setSessionPreset:AVCaptureSessionPreset1920x1080];
                    }
                    break;
                    
                case ResolutionModeFHD:
                    if ([_captureSession canSetSessionPreset:AVCaptureSessionPreset1920x1080]) {
                        [_captureSession setSessionPreset:AVCaptureSessionPreset1920x1080];
                    } else {
                        [_captureSession setSessionPreset:AVCaptureSessionPreset1280x720];
                    }
                    break;
                    
                case ResolutionModeHD:
                    if ([_captureSession canSetSessionPreset:AVCaptureSessionPreset1280x720]) {
                        [_captureSession setSessionPreset:AVCaptureSessionPreset1280x720];
                    } else {
                        [_captureSession setSessionPreset:AVCaptureSessionPresetLow];
                    }
                    break;
            }
        }
    }];
    
    NSLog(@"ResolutionMode %ld: session preset: %@", (long)resolutionMode, _captureSession.sessionPreset);
}

- (void)currentDeviceSupportResolutionMode:(ResolutionMode)resolutionMode
{
    [self device:_cameraDeviceInput.device supportWidth:[self widthForResolutionMode:resolutionMode] completionHandler:^(BOOL supported) {
        
    }];
}

- (int)widthForResolutionMode:(ResolutionMode)resolutionMode
{
    switch (resolutionMode) {
        case ResolutionMode4K:
            return 3840;
            break;
            
        case ResolutionModeFHD:
            return 1920;
            break;
            
        default://1280x720
            return 1280;
            break;
    }
}

- (ResolutionMode)resolutionModeFromSessionPreset:(AVCaptureSessionPreset)sessionPreset
{
    if (sessionPreset == AVCaptureSessionPreset3840x2160) {
        return ResolutionMode4K;
    } else if (sessionPreset == AVCaptureSessionPreset1920x1080) {
        return ResolutionModeFHD;
    } else {//1280x720 or less
        return ResolutionModeHD;
    }
}

- (AVCaptureSessionPreset)availableCaptureSessionPreset:(AVCaptureSessionPreset)resolutionMode
{
    if ([_captureSession canSetSessionPreset:AVCaptureSessionPreset3840x2160]) {
        return AVCaptureSessionPreset3840x2160;
    } else if ([_captureSession canSetSessionPreset:AVCaptureSessionPreset1920x1080]) {
        return AVCaptureSessionPreset1920x1080;
    } else if ([_captureSession canSetSessionPreset:AVCaptureSessionPreset1280x720]) {
        return AVCaptureSessionPreset1280x720;
    } else {
       return AVCaptureSessionPresetLow;
    }
}

- (void)device:(AVCaptureDevice *)device supportWidth:(int)width completionHandler:(void (^)(BOOL supported))handler
{
    [device.formats enumerateObjectsUsingBlock:^(AVCaptureDeviceFormat * _Nonnull format, NSUInteger index, BOOL * _Nonnull stop) {
        BOOL success = NO;
        CMVideoDimensions dimensions = CMVideoFormatDescriptionGetDimensions(format.formatDescription);
        if (dimensions.width == width) {
            success = YES;
            *stop = YES;
        }
        
        if (stop || index == device.formats.count -1) {
            handler(success);
        }
    }];
}

- (AVCaptureSessionPreset)supportedSessionPresetForDevice:(AVCaptureDevice *)device
{
    int width = CMVideoFormatDescriptionGetDimensions(device.formats.lastObject.formatDescription).width;
    if (width >= 3840) {
        return AVCaptureSessionPreset3840x2160;
    } else if (width >= 1920) {
        return AVCaptureSessionPreset1920x1080;
    } else {
        return AVCaptureSessionPreset1280x720;
    }
}

- (float)minISO
{
    return _cameraDeviceInput.device.activeFormat.minISO;
}

- (float)maxISO
{
    return _cameraDeviceInput.device.activeFormat.maxISO;
}

- (float)minTargetBias
{
    //float minTargetBias = _cameraDeviceInput.device.minExposureTargetBias;
    return  -TARGET_BIAS;//minTargetBias > TARGET_BIAS ? TARGET_BIAS : minTargetBias;
}

- (float)maxTargetBias
{
    //float maxTargetBias = _cameraDeviceInput.device.maxExposureTargetBias;
    return TARGET_BIAS;//maxTargetBias > -TARGET_BIAS ? -TARGET_BIAS : maxTargetBias;
}

- (float)minZoom {
    return MIN_ZOOM_FACTOR;
}

- (float)maxZoom {
    return _cameraDeviceInput.device.activeFormat.videoMaxZoomFactor * 0.25;
}

- (float)currentZoom
{
    return _cameraDeviceInput.device.videoZoomFactor;
}

- (CGFloat)lensPosition
{
    return _cameraDeviceInput.device.lensPosition;
}

- (BOOL)isFocusFocusModeLockedSupported {
    return [_cameraDeviceInput.device isFocusModeSupported:AVCaptureFocusModeLocked];
}

- (void)updateLensPosition:(CGFloat)position
{
    AVCaptureDevice *device = _cameraDeviceInput.device;
    NSError *error;
    if ([self isFocusFocusModeLockedSupported]) {
        if ([device lockForConfiguration:&error]) {
            [device setFocusModeLockedWithLensPosition:position completionHandler:^(CMTime syncTime) {
                NSLog(@"%s: %.2f", __func__, position);
            }];
            [device unlockForConfiguration];
        }
    }
    else {
        // TODO: Hide lens slider
    }
}

- (void)zoomCameraWithVelocity:(CGFloat)velocity
{
    CGFloat maxZoomFactor = _cameraDeviceInput.device.activeFormat.videoMaxZoomFactor;
    NSError *error;
    if ([_cameraDeviceInput.device lockForConfiguration:&error]) {
        CGFloat desiredZoomFactor = _cameraDeviceInput.device.videoZoomFactor +
        atan2f(velocity, PINCH_VELOCITY_FACTOR);
        _cameraDeviceInput.device.videoZoomFactor = MAX(1.0, MIN(desiredZoomFactor, maxZoomFactor));
        [_cameraDeviceInput.device unlockForConfiguration];
    }
    NSLog(@"%f", _cameraDeviceInput.device.videoZoomFactor);
}

- (void)zoomCameraWithZoomFactor:(CGFloat)zoomFactor
{
    NSError *error;
    if ([_cameraDeviceInput.device lockForConfiguration:&error]) {
        _cameraDeviceInput.device.videoZoomFactor = zoomFactor;
        [_cameraDeviceInput.device unlockForConfiguration];
    }
    NSLog(@"%f", _cameraDeviceInput.device.videoZoomFactor);
}

- (void)setDelegate:(id<IDCaptureSessionCoordinatorDelegate>)delegate callbackQueue:(dispatch_queue_t)delegateCallbackQueue
{
    if (delegate && (delegateCallbackQueue == NULL)) {
        @throw [NSException exceptionWithName:NSInvalidArgumentException reason:@"Caller must provide a delegateCallbackQueue" userInfo:nil];
    }
    @synchronized(self)
    {
        _delegate = delegate;
        if (delegateCallbackQueue != _delegateCallbackQueue) {
            _delegateCallbackQueue = delegateCallbackQueue;
        }
    }
}

- (AVCaptureVideoPreviewLayer *)previewLayer
{
    if(!_previewLayer && _captureSession){
        _previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:_captureSession];
    }
    return _previewLayer;
}

- (void)startRunning
{
    dispatch_sync( _sessionQueue, ^{
        [_captureSession startRunning];
    } );
}

- (void)stopRunning
{
    dispatch_sync( _sessionQueue, ^{
        // the captureSessionDidStopRunning method will stop recording if necessary as well, but we do it here so that the last video and audio samples are better aligned
        [self stopRecording]; // does nothing if we aren't currently recording
        [_captureSession stopRunning];
    } );
}


- (void)startRecording
{
    //overwritten by subclass
}

- (void)stopRecording
{
    //overwritten by subclass
}


- (void)enableRecordAudio:(BOOL)enabled
{
    //overwritten by subclass
}



#pragma mark - Capture Session Setup


- (void)setupCaptureSession
{
    NSError *error;
    AVCaptureDeviceInput *deviceInput;
    
    [_captureSession beginConfiguration];
//    if(![self addDefaultCameraInputToCaptureSession:captureSession]){
//        NSLog(@"failed to add camera input to capture session");
//    }
    for (AVCaptureDevice *device in _videoDevices) {
        if (device.position == AVCaptureDevicePositionBack) {
            deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
            NSLog(@"Error: %@", error);
            //[[AVCaptureDeviceInput alloc] initWithDevice:device error:&error];
            if ([self addInput:deviceInput toCaptureSession:_captureSession]) {
                _cameraDeviceInput = deviceInput;
            }
            break;
        }
    }
    if (![self addDefaultMicInputToCaptureSession:_captureSession]) {
        NSLog(@"failed to add mic input to capture session");
    }
    [_captureSession commitConfiguration];
}

- (BOOL)addDefaultCameraInputToCaptureSession:(AVCaptureSession *)captureSession
{
    NSError *error;
    AVCaptureDeviceInput *cameraDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] error:&error];

    if (error) {
        NSLog(@"error configuring camera input: %@", [error localizedDescription]);
        return NO;
    } else {
        BOOL success = [self addInput:cameraDeviceInput toCaptureSession:captureSession];
        _cameraDeviceInput = cameraDeviceInput;
        return success;
    }
}

//Not used in this project, but illustration of how to select a specific camera
- (BOOL)addCameraAtPosition:(AVCaptureDevicePosition)position toCaptureSession:(AVCaptureSession *)captureSession
{
    NSError *error;
//    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *cameraDeviceInput;
    for (AVCaptureDevice *device in _videoDevices) {
        if (device.position == position) {
            cameraDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:device error:&error];
        }
    }
    if(!cameraDeviceInput){
        NSLog(@"No capture device found for requested position");
        return NO;
    }
    
    if(error){
        NSLog(@"error configuring camera input: %@", [error localizedDescription]);
        return NO;
    } else {
        BOOL success = [self addInput:cameraDeviceInput toCaptureSession:captureSession];
        _cameraDeviceInput = cameraDeviceInput;
        return success;
    }
}

- (BOOL)addDefaultMicInputToCaptureSession:(AVCaptureSession *)captureSession
{
    NSError *error;
    AVCaptureDeviceInput *micDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio] error:&error];
    if(error){
        NSLog(@"error configuring mic input: %@", [error localizedDescription]);
        return NO;
    } else {
        BOOL success = [self addInput:micDeviceInput toCaptureSession:captureSession];
        return success;
    }
}

- (BOOL)addInput:(AVCaptureDeviceInput *)input toCaptureSession:(AVCaptureSession *)captureSession
{
    if([captureSession canAddInput:input]){
        [captureSession addInput:input];
        return YES;
    } else {
        NSLog(@"can't add input: %@", [input description]);
    }
    return NO;
}


- (BOOL)addOutput:(AVCaptureOutput *)output toCaptureSession:(AVCaptureSession *)captureSession
{
    if([captureSession canAddOutput:output]){
        [captureSession addOutput:output];
        return YES;
    } else {
        NSLog(@"can't add output: %@", [output description]);
    }
    return NO;
}


#pragma mark - Methods discussed in the article but not used in this demo app

- (void)setFrameRateWithDuration:(CMTime)frameDuration OnCaptureDevice:(AVCaptureDevice *)device
{
    NSError *error;
    NSArray *supportedFrameRateRanges = [device.activeFormat videoSupportedFrameRateRanges];
    BOOL frameRateSupported = NO;
    for(AVFrameRateRange *range in supportedFrameRateRanges){
        if(CMTIME_COMPARE_INLINE(frameDuration, >=, range.minFrameDuration) && CMTIME_COMPARE_INLINE(frameDuration, <=, range.maxFrameDuration)){
            frameRateSupported = YES;
        }
    }
    
    if(frameRateSupported && [device lockForConfiguration:&error]){
        [device setActiveVideoMaxFrameDuration:frameDuration];
        [device setActiveVideoMinFrameDuration:frameDuration];
        [device unlockForConfiguration];
    }
}


- (void)listCamerasAndMics
{
    NSLog(@"%@", [[AVCaptureDevice devices] description]);
    NSError *error;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if(error){
        NSLog(@"%@", [error localizedDescription]);
    }
    [audioSession setActive:YES error:&error];
    
    NSArray *availableAudioInputs = [audioSession availableInputs];
    NSLog(@"audio inputs: %@", [availableAudioInputs description]);
    for(AVAudioSessionPortDescription *portDescription in availableAudioInputs){
        NSLog(@"data sources: %@", [[portDescription dataSources] description]);
    }
    if([availableAudioInputs count] > 0){
        AVAudioSessionPortDescription *portDescription = [availableAudioInputs firstObject];
        if([[portDescription dataSources] count] > 0){
            NSError *error;
            AVAudioSessionDataSourceDescription *dataSource = [[portDescription dataSources] lastObject];
            
            [portDescription setPreferredDataSource:dataSource error:&error];
            [self logError:error];
            
            [audioSession setPreferredInput:portDescription error:&error];
            [self logError:error];

            NSArray *availableAudioInputs = [audioSession availableInputs];
            NSLog(@"audio inputs: %@", [availableAudioInputs description]);
        
        }
    }
}

- (void)logError:(NSError *)error
{
    if(error){
        NSLog(@"%@", [error localizedDescription]);
    }
}

- (void)configureFrontMic
{
    NSError *error;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if(error){
        NSLog(@"%@", [error localizedDescription]);
    }
    [audioSession setActive:YES error:&error];
    
    NSArray* inputs = [audioSession availableInputs];
    AVAudioSessionPortDescription *builtInMic = nil;
    for (AVAudioSessionPortDescription* port in inputs){
        if ([port.portType isEqualToString:AVAudioSessionPortBuiltInMic]){
            builtInMic = port;
            break;
        }
    }
    
    for (AVAudioSessionDataSourceDescription* source in builtInMic.dataSources){
        if ([source.orientation isEqual:AVAudioSessionOrientationFront]){
            [builtInMic setPreferredDataSource:source error:nil];
            [audioSession setPreferredInput:builtInMic error:&error];
            break;
        }
    }
}


@end
