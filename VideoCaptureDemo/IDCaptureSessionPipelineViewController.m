//
//  IDCaptureSessionPipelineViewController.m
//  VideoCaptureDemo
//
//  Created by Adriaan Stellingwerff on 9/04/2015.
//  Copyright (c) 2015 Infoding. All rights reserved.
//

#import "IDCaptureSessionPipelineViewController.h"
#import "IDCaptureSessionAssetWriterCoordinator.h"
#import "IDCaptureSessionMovieFileOutputCoordinator.h"
#import "IDFileManager.h"
#import "IDPermissionsManager.h"
#import <Photos/Photos.h>

//TODO: add backgrounding stuff

@interface IDCaptureSessionPipelineViewController () <IDCaptureSessionCoordinatorDelegate>

@property (nonatomic, strong) IDCaptureSessionCoordinator *captureSessionCoordinator;
@property (nonatomic, retain) IBOutlet UIButton *cameraButton;
@property (nonatomic, retain) IBOutlet UIButton *recordButton;
@property (nonatomic, retain) IBOutlet UIButton *recordAudioButton;
@property (nonatomic, retain) IBOutlet UIButton *focusButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *exposureButton;
@property (nonatomic, retain) IBOutlet UIButton *resolutionButton;
@property (nonatomic, strong) IBOutlet UISlider *lensSlider;
@property (weak, nonatomic) IBOutlet UISlider *biasSlider;
@property (weak, nonatomic) IBOutlet UIView *biasView;
@property (weak, nonatomic) IBOutlet UISlider *zoomSlider;

@property (nonatomic, assign) BOOL recording;
@property (nonatomic, assign) BOOL isAudioEnabled;
@property (nonatomic, assign) BOOL dismissing;

@property (nonatomic, assign) FocusMode currentFocusMode;
@property (nonatomic, assign) ExposureMode currentExposureMode;
@property (nonatomic, assign) ResolutionMode currentResolutionMode;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL blinkStatus;
@property (weak, nonatomic) IBOutlet UIView *tallyView;

@property (nonatomic, assign) BOOL isPanelHidden;

@end

@implementation IDCaptureSessionPipelineViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupWithPipelineMode:PipelineModeAssetWriter];
    _isPanelHidden = NO;
    _isAudioEnabled = YES;
    _currentFocusMode = FocusModeContinuous;
    _currentExposureMode = ExposureModeContinuous;
    _currentResolutionMode = ResolutionMode4K;
    [_captureSessionCoordinator changeVideoResolutionWithMode:_currentResolutionMode];
    [_resolutionButton setImage:[UIImage imageNamed:[_captureSessionCoordinator currentResolutionImageName]] forState:UIControlStateNormal];
    
    UIInterfaceOrientation statusBarOrientation = [UIApplication sharedApplication].statusBarOrientation;
    AVCaptureVideoOrientation initialVideoOrientation = AVCaptureVideoOrientationPortrait;
    if (statusBarOrientation != UIInterfaceOrientationUnknown) {
        initialVideoOrientation = (AVCaptureVideoOrientation)statusBarOrientation;
    }
    [_captureSessionCoordinator previewLayerWithOrientation:(AVCaptureVideoOrientation)initialVideoOrientation];
    _lensSlider.hidden = YES;
    
    _biasSlider.value = 0.0;
    _biasSlider.minimumValue = [_captureSessionCoordinator minTargetBias];
    _biasSlider.maximumValue = [_captureSessionCoordinator maxTargetBias];
    _lensSlider.transform = CGAffineTransformRotate(_lensSlider.transform, 270.0 / 180 * M_PI);
    
    _zoomSlider.minimumValue = [_captureSessionCoordinator minZoom];
    _zoomSlider.maximumValue = [_captureSessionCoordinator maxZoom];
    _zoomSlider.value = [_captureSessionCoordinator currentZoom];
    _zoomSlider.transform = CGAffineTransformRotate(_zoomSlider.transform, 270.0 / 180 * M_PI);
    
//    [_lensSlider setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    
//    _lensSlider.transform = CGAffineTransformMakeRotation(-M_PI_2);
//    [_lensSlider.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
//    [_lensSlider.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = YES;
    
//    [self viewDidLayoutSubviews];
//    [_lensSlider.heightAnchor constraintEqualToConstant:100].active = YES;
    
//    [_lensSlider.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:20].active = YES;
//    [_lensSlider.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-20].active = YES;
//    [_lensSlider.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:-20].active = YES;

}

- (void)setupWithPipelineMode:(PipelineMode)mode
{
    //Let's check permissions for microphone and camera access before we get started
    [self checkPermissions];
    
    switch (mode) {
        case PipelineModeMovieFileOutput:
            _captureSessionCoordinator = [IDCaptureSessionMovieFileOutputCoordinator new];
            break;
        case PipelineModeAssetWriter:
            _captureSessionCoordinator = [IDCaptureSessionAssetWriterCoordinator new];
            break;
        default:
            break;
    }
    [_captureSessionCoordinator setDelegate:self callbackQueue:dispatch_get_main_queue()];
    [self configureInterface];
}

- (IBAction)toggleRecording:(id)sender
{
    if(_recording){
        [_captureSessionCoordinator stopRecording];
        _resolutionButton.enabled = YES;
        _cameraButton.enabled = YES;
    } else {
        _resolutionButton.enabled = NO;
        _cameraButton.enabled = NO;
        // Disable the idle timer while recording
        [UIApplication sharedApplication].idleTimerDisabled = YES;
        
        self.recordButton.enabled = NO; // re-enabled once recording has finished starting
//        [self.recordButton setTitle:@"Stop" forState:UIControlStateNormal];
        [self.recordButton setImage:[UIImage imageNamed:@"record"] forState:UIControlStateNormal];
        
        [self.captureSessionCoordinator startRecording];
        [self startTally];
        
        _recording = YES;
    }
}

- (IBAction)closeCamera:(id)sender
{
    //TODO: tear down pipeline
    if(_recording){
        _dismissing = YES;
        [_captureSessionCoordinator stopRecording];
    } else {
        [self stopPipelineAndDismiss];
    }
}

- (IBAction)focusAndExposeTap:(UIGestureRecognizer*)gestureRecognizer
{
    if (_currentFocusMode != FocusModeManual) {
        NSLog(@"%@", NSStringFromCGPoint([gestureRecognizer locationInView:gestureRecognizer.view]));
        [_captureSessionCoordinator focusWithMode:FocusModeAuto exposeWithMode:ExposureModeAuto atPointOfInterest:[gestureRecognizer locationInView:gestureRecognizer.view] monitorSubjectAreaChange:YES];
    } else {
        NSLog(@"Manual focus");
    }
}

/*- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (_currentFocusMode == FocusModeContinuous) {
        UITouch *touch = touches.allObjects.lastObject;
        CGFloat touchPercent = [touch locationInView:self.view].x / [UIScreen mainScreen].bounds.size.width;
        NSLog(@"touchPercent: %f, monitorSubjectAreaChange: %@", touchPercent, [self shouldMonitorSubjectAreaChange] ? @"YES" : @"NO");
        
        [_captureSessionCoordinator focusWithMode:_currentFocusMode exposeWithMode:AVCaptureExposureModeAutoExpose atPointOfInterest:[touch locationInView:self.view] monitorSubjectAreaChange:[self shouldMonitorSubjectAreaChange]];
        
    }
}*/

- (IBAction)toggleRecordingAudio:(id)sender
{
    if(_isAudioEnabled) {
        _isAudioEnabled = NO;
        [_captureSessionCoordinator enableRecordAudio:_isAudioEnabled];
//        [_recordAudioButton setTitle:@"Unmute Audio" forState:UIControlStateNormal];
        [_focusButton setImage:[UIImage imageNamed:@"microphone-off"] forState:UIControlStateNormal];
    } else {
        _isAudioEnabled = YES;
        [_captureSessionCoordinator enableRecordAudio:_isAudioEnabled];
//        [_recordAudioButton setTitle:@"Mute Audio" forState:UIControlStateNormal];
        [_focusButton setImage:[UIImage imageNamed:@"microphone-on"] forState:UIControlStateNormal];
    }
}

- (IBAction)changeFocusMode:(id)sender
{
    _currentFocusMode = _currentFocusMode == FocusModeContinuous ? FocusModeManual : FocusModeContinuous;
    
    if (_currentFocusMode == FocusModeContinuous) {
//        [_focusButton setTitle:@"Auto" forState:UIControlStateNormal];
        [_focusButton setImage:[UIImage imageNamed:@"focus-on"] forState:UIControlStateNormal];
        _lensSlider.hidden = YES;
    } else {
//        [_focusButton setTitle:@"Manual" forState:UIControlStateNormal];
        [_focusButton setImage:[UIImage imageNamed:@"focus-off"] forState:UIControlStateNormal];
        _lensSlider.value = [_captureSessionCoordinator lensPosition];
        _lensSlider.hidden = NO;
    }
    
    CGPoint point = CGPointMake(0.5, 0.5);
    [_captureSessionCoordinator focusWithMode:_currentFocusMode exposeWithMode:ExposureModeContinuous atPointOfInterest:point monitorSubjectAreaChange:[self shouldMonitorSubjectAreaChange]];
}

- (IBAction)changeResolution:(id)sender {
    if (_currentResolutionMode == ResolutionMode4K) {
        _currentResolutionMode = ResolutionModeFHD;
    } else if (_currentResolutionMode == ResolutionModeFHD) {
        _currentResolutionMode = ResolutionModeHD;
    } else {
        _currentResolutionMode = ResolutionMode4K;
    }
    
    [_captureSessionCoordinator changeVideoResolutionWithMode:_currentResolutionMode];
    [self configureInterface];
}

- (IBAction)changeExposureMode:(id)sender
{
    _currentExposureMode = _currentExposureMode == ExposureModeContinuous ? ExposureModeCustom : ExposureModeContinuous;
    
    if (_currentExposureMode == ExposureModeContinuous) {
        _exposureButton.title = @"Auto Exposure";
    } else {
        _exposureButton.title = @"Manual Exposure";
    }
}

- (IBAction)updateTargetBias:(id)sender
{
    float value = roundf(_biasSlider.value / 1.0) * 1.0;
    [_biasSlider setValue:value animated:YES];
    [_captureSessionCoordinator exposureWithMode:_currentExposureMode targetBias:_biasSlider.value duration:AVCaptureExposureDurationCurrent ISO:0];
}

- (IBAction)updateZoom:(id)sender
{
    [_captureSessionCoordinator zoomCameraWithZoomFactor:_zoomSlider.value];
}

- (BOOL)shouldMonitorSubjectAreaChange
{
    return _currentFocusMode != FocusModeManual;
}

- (IBAction)changeCamera:(id)sender
{
    [_captureSessionCoordinator changeCamera];
    [_resolutionButton setImage:[UIImage imageNamed:[_captureSessionCoordinator currentResolutionImageName]] forState:UIControlStateNormal];
    
    if ([_captureSessionCoordinator isFocusFocusModeLockedSupported]) {
        if (_currentFocusMode == FocusModeManual) {
//            [_focusButton setTitle:@"Manual" forState:UIControlStateNormal];
            [_focusButton setImage:[UIImage imageNamed:@"focus-off"] forState:UIControlStateNormal];
            _lensSlider.hidden = _isPanelHidden;
        } else {
//            [_focusButton setTitle:@"Auto" forState:UIControlStateNormal];
            [_focusButton setImage:[UIImage imageNamed:@"focus-on"] forState:UIControlStateNormal];
        }
        _focusButton.enabled = YES;
    } else {
//        [_focusButton setTitle:@"Auto" forState:UIControlStateNormal];
        [_focusButton setImage:[UIImage imageNamed:@"focus-on"] forState:UIControlStateNormal];
        _focusButton.enabled = NO;
        _lensSlider.hidden = YES;
    }
}

- (IBAction)updateLensPosition:(id)sender
{
    [_captureSessionCoordinator updateLensPosition:_lensSlider.value];
}
- (IBAction)pinchToZoom:(UIPinchGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateChanged || sender.state == UIGestureRecognizerStateBegan) {
        [_captureSessionCoordinator zoomCameraWithVelocity:sender.velocity];
    }
}

- (IBAction)hidePanel:(id)sender {
    _isPanelHidden = _isPanelHidden ? NO : YES;
    
    _cameraButton.hidden = _isPanelHidden;
    _recordAudioButton.hidden = _isPanelHidden;
    _focusButton.hidden = _isPanelHidden;
    _zoomSlider.hidden = _isPanelHidden;
    _biasSlider.hidden = _isPanelHidden;
    _biasView.hidden = _isPanelHidden;
    
    if (_currentFocusMode == FocusModeManual) {
        _lensSlider.hidden = _isPanelHidden;
    }
}

#pragma mark - Private methods

- (void)configureInterface
{
    _zoomSlider.minimumValue = [_captureSessionCoordinator minZoom];
    _zoomSlider.maximumValue = [_captureSessionCoordinator maxZoom];
    _zoomSlider.value = [_captureSessionCoordinator currentZoom];
    [_resolutionButton setImage:[UIImage imageNamed:[_captureSessionCoordinator currentResolutionImageName]] forState:UIControlStateNormal];
    
    AVCaptureVideoPreviewLayer *previewLayer = [_captureSessionCoordinator previewLayer];
    previewLayer.frame = self.view.bounds;
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill; // Preview layer fill all screen
    [self.view.layer insertSublayer:previewLayer atIndex:0];
    
    [_captureSessionCoordinator startRunning];
}

- (void)stopPipelineAndDismiss
{
    [_captureSessionCoordinator stopRunning];
    [self dismissViewControllerAnimated:YES completion:nil];
    _dismissing = NO;
}

- (void)checkPermissions
{
    IDPermissionsManager *pm = [IDPermissionsManager new];
    [pm checkCameraAuthorizationStatusWithBlock:^(BOOL granted) {
        if(!granted){
            NSLog(@"we don't have permission to use the camera");
        }
    }];
    [pm checkMicrophonePermissionsWithBlock:^(BOOL granted) {
        if(!granted){
            NSLog(@"we don't have permission to use the microphone");
        }
    }];
}

#pragma mark - IDCaptureSessionCoordinatorDelegate methods

- (void)coordinatorDidBeginRecording:(IDCaptureSessionCoordinator *)coordinator
{
    _recordButton.enabled = YES;
}

- (void)coordinator:(IDCaptureSessionCoordinator *)coordinator didFinishRecordingToOutputFileURL:(NSURL *)outputFileURL error:(NSError *)error
{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    [_recordButton setTitle:@"Record" forState:UIControlStateNormal];
    [self stopTally];
    _recording = NO;
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (status == PHAuthorizationStatusAuthorized) {
            //Do something useful with the video file available at the outputFileURL
            IDFileManager *fm = [IDFileManager new];
            [fm copyFileToCameraRoll:outputFileURL];
            
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Library access disabled"
                                                                               message:@"This app doesn't have permission to access the photo library, please go to the Settings app > Privacy > Camera and enable access."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Settings"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action)
                                           {
                                               [[UIApplication sharedApplication] openURL:
                                                [NSURL URLWithString:UIApplicationOpenSettingsURLString]
                                                                                  options:@{}
                                                                        completionHandler:nil];
                                           }];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction * _Nonnull action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                         }];
                
                [alert addAction:settings];
                [alert addAction:cancel];
                alert.preferredAction = settings;
                
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert
                                                                                                 animated:YES
                                                                                               completion:nil];
            });
        }
    }];
    
    
    //Dismiss camera (when user taps cancel while camera is recording)
    if (_dismissing) {
        [self stopPipelineAndDismiss];
    }
}

#pragma mark - Screen Rotation

- (BOOL)shouldAutorotate
{
    return _recording ? NO : YES;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    /*UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    
    if (UIDeviceOrientationIsPortrait(deviceOrientation) || UIDeviceOrientationIsLandscape(deviceOrientation)) {
        [_captureSessionCoordinator previewLayerWithOrientation:(AVCaptureVideoOrientation)deviceOrientation];
    }*/
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        UIInterfaceOrientation statusBarOrientation = [[UIApplication sharedApplication] statusBarOrientation];
        [_captureSessionCoordinator previewLayerWithOrientation:(AVCaptureVideoOrientation)statusBarOrientation];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
    }];
}

#pragma mark - Tally

- (void)startTally
{
    _blinkStatus = YES;
    [self blinkView];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                              target:self
                                            selector:@selector(blinkView)
                                            userInfo:nil
                                             repeats:YES];
}

- (void)stopTally
{
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _tallyView.hidden = YES;
}

- (void)blinkView
{
    if (_blinkStatus) {
        _blinkStatus = NO;
    } else {
        _blinkStatus = YES;
    }
    _tallyView.hidden = _blinkStatus;
}

@end
