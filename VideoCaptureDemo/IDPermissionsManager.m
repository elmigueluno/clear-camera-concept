//
//  IDCameraPermissionsManager.m
//  VideoCameraDemo
//
//  Created by Adriaan Stellingwerff on 10/03/2014.
//  Copyright (c) 2014 Infoding. All rights reserved.
//

#import "IDPermissionsManager.h"
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>


@interface IDPermissionsManager ()


@end

@implementation IDPermissionsManager

- (void)checkMicrophonePermissionsWithBlock:(void(^)(BOOL granted))block
{
    NSString *mediaType = AVMediaTypeAudio;
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
    if(!granted){
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Microphone disabled"
                                                                               message:@"To enable sound recording with your video please go to the Settings app > Privacy > Microphone and enable access."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Settings"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action)
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]
                                                       options:@{}
                                             completionHandler:nil];
                }];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction * _Nonnull action)
                {
                    [alert dismissViewControllerAnimated:YES completion:nil];
                }];
                
                [alert addAction:settings];
                [alert addAction:cancel];
                alert.preferredAction = settings;
                
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert
                                                                                                 animated:YES
                                                                                               completion:nil];
            });
        }
        if(block != nil)
            block(granted);
    }];
}


- (void)checkCameraAuthorizationStatusWithBlock:(void(^)(BOOL granted))block
{
	NSString *mediaType = AVMediaTypeVideo;
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        if (!granted){
            //Not granted access to mediaType
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Camera disabled"
                                                                               message:@"This app doesn't have permission to use the camera, please go to the Settings app > Privacy > Camera and enable access."
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Settings"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action)
                                           {
                                               [[UIApplication sharedApplication] openURL:
                                                [NSURL URLWithString:UIApplicationOpenSettingsURLString]
                                                                                  options:@{}
                                                                        completionHandler:nil];
                                           }];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                                 style:UIAlertActionStyleCancel
                                                               handler:^(UIAlertAction * _Nonnull action)
                                         {
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                         }];
                
                [alert addAction:settings];
                [alert addAction:cancel];
                alert.preferredAction = settings;
                
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alert
                                                                                                 animated:YES
                                                                                               completion:nil];
            });
        }
        if(block)
            block(granted);
    }];
}


@end
